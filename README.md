# Distroless Unbound Multiarch Docker Image

[Unbound](https://unbound.net/) is a validating, recursive, and caching DNS resolver.

Note that this image is [distroless](https://github.com/GoogleContainerTools/distroless)!

> "Distroless" images contain only your application and its runtime dependencies. They do not contain package managers, shells or any other programs you would expect to find in a standard Linux distribution.

By default, the `unbound` configuration in this repo `rootfs_overlay/etc/unbound/unbound.conf` is using DNS-over-TLS with Cloudflare Malware Filtering DNS servers.

## Usage

Run a recursive dns server on host port 53 with the default configuration.

```bash
docker run --name unbound \
  -p 53:53/tcp -p 53:53/udp \
  <IMAGE_NAME>
```

Optionally mount [custom configuration](https://unbound.docs.nlnetlabs.nl/en/latest/manpages/unbound.conf.html) from a host directory.
Files must be readable by user/group `101:102` or world.

```bash
docker run --name unbound \
  -p 53:53/tcp -p 53:53/udp \
  -v /path/to/config:/etc/unbound/custom.conf.d \
  <IMAGE_NAME>
```

## Build

```bash
# optionally update root hints before building
# Root hints are not expected to change frequently
rm rootfs_overlay/etc/unbound/root.hints
wget https://www.internic.net/domain/named.root -O rootfs_overlay/etc/unbound/root.hints
```

Also, update the root keys for DNSSEC: [Obtaining Initial Anchor](https://nlnetlabs.nl/documentation/unbound/howto-anchor/)

```bash
# enable docker buildkit and experimental mode
export DOCKER_BUILDKIT=1
export DOCKER_CLI_EXPERIMENTAL=enabled

# build local image for native platform
docker build . --tag <TAG>

# cross-build for another platform
docker build . --tag <TAG> --platform linux/arm/v7
```

## Test

```bash
# enable QEMU for arm emulation
docker run --rm --privileged multiarch/qemu-user-static:5.2.0-2 --reset -p yes

# run a detached unbound container
docker run --rm -d --name unbound <IMAGE_NAME>

# run dig with dnssec to test an example NOERROR endpoint
docker exec unbound dig sigok.verteiltesysteme.net @127.0.0.1 +dnssec

# run dig with dnssec to test an example SERVFAIL endpoint
docker exec unbound dig sigfail.verteiltesysteme.net @127.0.0.1 +dnssec

# stop and remove the container
docker stop unbound
```

## Acknowledgements

- Original software is by NLnet Labs: <https://unbound.net>.
- This work was based on Kyle Harding's (from Balena team) repos and modified accordingly for specific use cases.

## Resources
You might find the following resources useful:
- [Kyle Harding's Unbound Docker Repo](https://github.com/klutchell/unbound-docker)
- [Kyle Harding's Archived Unbound Docker Repo](https://github.com/klutchell/unbound)
- [Matthew Vance2s Unbound Docker Repo](https://github.com/MatthewVance/unbound-docker)
